"""
Parses the input and normalizes the args
"""

from enum import Enum, auto
from typing import List


class CommandType(Enum):
    """
    Enum of the various commands that can be compiled
    """
    C_ARITHMETIC = auto()
    C_PUSH = auto()
    C_POP = auto()
    C_LABEL = auto()
    C_GOTO = auto()
    C_IF = auto()
    C_FUNCTION = auto()
    C_RETURN = auto()
    C_CALL = auto()


class Parser():
    """
    Class that keeps the state of the file and impliments the file IO ->
    command conversion, as well as normalizing the input
    """

    segments = set(['argument', 'local', 'static', 'constant', 'this',
                    'that', 'pointer', 'temp'])

    def __init__(self, filename: str) -> None:
        self.filename = filename
        self.file_steam = None
        self.lines: List[str] = []
        self.current_line: str = ''

        with open(self.filename) as file:
            all_lines: List[str] = file.readlines()

        for line in all_lines:
            test_line: str = line.replace('\n', '').strip().lower()

            # remove comments
            comment_index = test_line.find('//')
            if comment_index != -1:
                test_line = test_line[:comment_index].strip()
            if test_line:
                self.lines.append(test_line)

    def has_more_commands(self) -> bool:
        """
        Are there more commands in the input

        :return: True if more commands, False otherwise
        """
        return len(self.lines) != 0

    def advance(self):
        """
        Reads the next command from the input and makes it the current command
        """
        if self.has_more_commands():
            temp_str = self.lines.pop(0)
            self.current_line = temp_str.split()

    def get_command_type(self) -> CommandType:
        """
        Returns the type of the current VM command, C_ARITHMETIC is returned
        for all the arithmetic commands
        """
        return_type: CommandType = CommandType.C_ARITHMETIC  # default
        if 'pop' in self.current_line:
            return_type = CommandType.C_POP
        elif 'push' in self.current_line:
            return_type = CommandType.C_PUSH
        elif self.segments.intersection(self.current_line):
            return_type = CommandType.C_LABEL
        elif 'goto' in self.current_line:
            return_type = CommandType.C_GOTO
        elif 'if' in self.current_line:
            return_type = CommandType.C_IF
        elif 'function' in self.current_line:
            return_type = CommandType.C_FUNCTION
        elif 'return' in self.current_line:
            return_type = CommandType.C_RETURN
        elif 'call' in self.current_line:
            return_type = CommandType.C_CALL

        return return_type

    def get_arg1(self) -> str:
        """
        Returns the first argument of the current command. In the case of
        C_ARITHMETIC, the command itself(add, sub, etc) is returned.
        Should not be called if the current command is C_RETURN

        :return: First argument of the current command
        """
        if self.get_command_type() == CommandType.C_ARITHMETIC:
            return self.current_line[0]

        return self.current_line[1]

    def get_arg2(self) -> int:
        """
        Returns the second argument of the current command. Should be
        called only if the current command is C_PUSH, C_POP, C_FUNCTION,
        or C_CALL

        :return: Second argument of the current command
        """
        if self.get_command_type() in [CommandType.C_PUSH, CommandType.C_POP,
                                       CommandType.C_FUNCTION, CommandType.C_CALL]:
            return int(self.current_line[2])

        raise ParserException(f'get_arg2 called with {self.get_command_type()}'
                              ', which is unsupported')


class ParserException(Exception):
    """
    General exception for the parser class, errors with parsing the file
    """
    pass
