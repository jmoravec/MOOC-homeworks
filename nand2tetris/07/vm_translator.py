"""
Takes a Hack VM file and outputs the Hack Assembly file
"""
import os
import argparse

from vm_parser.parser import Parser, CommandType
from code_writer.code_writer import CodeWriter


def main():
    """
    Main program loop
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('filename', type=str, help='VM input file')
    parser.add_argument('--output', type=str, default=None,
                        help='File to write output too, defaults to [filename].asm')
    args = parser.parse_args()

    if args.output is None:
        new_file_name = '{}.asm'.format(os.path.splitext(args.filename[0]))
    else:
        new_file_name = args.output

    translator = CodeWriter(new_file_name)
    all_commands: str = ''
    # import pudb; pu.db

    main_parser = Parser(args.filename)
    while main_parser.has_more_commands():
        main_parser.advance()
        command_type = main_parser.get_command_type()
        all_commands += f'//{main_parser.current_line}\n'
        print(main_parser.current_line)
        if command_type == CommandType.C_ARITHMETIC:
            all_commands += translator.get_arithmetic(main_parser.get_arg1())
        elif command_type in [CommandType.C_POP, CommandType.C_PUSH]:
            all_commands += translator.get_push_pop(command_type,
                                                    main_parser.get_arg1(),
                                                    main_parser.get_arg2())

    with open(new_file_name, 'w') as file:
        file.write(all_commands)


if __name__ == '__main__':
    main()
