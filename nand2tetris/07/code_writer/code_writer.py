"""
Module for methods that convert the VM commands to Hack assembly language
"""
from vm_parser.parser import CommandType


class CodeWriter():
    """
    Class that has methods to convert MV commands to Hack assembly
    """
    segment_translation = {
        'argument': 'ARG',
        'local': 'LCL',
        'this': 'THIS',
        'that': 'THAT',
        'pointer': 'R3',
        'temp': 'R5'
    }

    def __init__(self, filename: str) -> None:
        self.filename: str = filename
        self.arith_jump: int = 0

    @classmethod
    def _pop(cls) -> str:
        """
        Helper method that pops the stack in assembly
        """
        return '@SP\nAM=M-1\nD=M\n'

    @classmethod
    def _push_what(cls, what: str) -> str:
        return f'@SP\nA=M\nM={what}\n@SP\nM=M+1\n'

    @classmethod
    def _push_d(cls) -> str:
        """
        Helper method that pushs D to the stack
        """
        return cls._push_what('D')

    def get_arithmetic(self, command: str) -> str:
        """
        Converts add, sub, neg, eq, gt, lt, and, or, not to assembly
        """
        vm_command = ''
        if command in ['add', 'sub', 'and', 'or']:
            if 'add' in command:
                opp = '+'
            elif 'sub' in command:
                opp = '-'
            elif 'and' in command:
                opp = '&'
            elif 'or' in command:
                opp = '|'

            vm_command = self._pop()
            vm_command += f'A=A-1\nD=M{opp}D\n@SP\nM=M-1\n'
            vm_command += self._push_d()
        elif command in ['eq', 'gt', 'lt']:
            jump_location = f'ARITH_JUMP.{self.arith_jump}'
            self.arith_jump += 1
            jump_command: str
            if 'eq' in command:
                jump_command = 'JEQ'
            elif 'gt' in command:
                jump_command = 'JGT'
            else:
                jump_command = 'JLT'

            vm_command = self._pop()
            vm_command += 'A=A-1\nD=M-D\n@SP\nM=M-1\n'
            vm_command += f'@{jump_location}\nD;{jump_command}\n'
            # false statement
            vm_command += self._push_what('0')
            vm_command += f'@{jump_location}.END\n0;JMP\n'
            # true statement
            vm_command += f'({jump_location})\n' + self._push_what('-1')
            vm_command += f'({jump_location}.END)\n'
        elif command in ['neg', 'not']:
            if 'neg' in command:
                opp = '-'
            else:
                opp = '!'
            vm_command = f'@SP\nA=M-1\nM={opp}M\n'

        return vm_command

    def _get_segment_value(self, segment: str, index: int) -> str:
        segment_name = self.segment_translation[segment]
        if segment in ['pointer', 'temp']:
            location = 'A'
        else:
            location = 'M'

        return f'@{index}\nD=A\n@{segment_name}\nA={location}+D\nD=M\n'

    def _get_segment_index(self, segment: str, index: int) -> str:
        if segment in ['temp', 'pointer']:
            location = 'A'
        else:
            location = 'M'
        segment_name = self.segment_translation[segment]
        return f'@{index}\nD=A\n@{segment_name}\nD={location}+D\n'

    def get_push_pop(self, command_type: CommandType, segment: str, index: int) -> str:
        """
        Gets the assembly code that is the translation of push or pop
        """
        vm_command: str = ''
        if command_type == CommandType.C_PUSH:
            if segment in self.segment_translation:
                vm_command += self._get_segment_value(segment, index)
            elif 'constant' in segment:
                vm_command += f'@{index}\nD=A\n'
            else:  # static
                vm_command += f'@{self.filename}.{index}\nD=M\n'

            vm_command += self._push_d()
        else:
            if segment in self.segment_translation:
                vm_command += self._get_segment_index(segment, index)
                vm_command += '@R13\nM=D\n'
                vm_command += self._pop()
                vm_command += '@R13\nA=M\nM=D\n'
            else:  # static
                vm_command += self._pop()
                vm_command += f'@{self.filename}.{index}\nM=D\n'

        return vm_command
