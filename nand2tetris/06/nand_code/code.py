"""
Code Module for the nand2tetris assembler that
translates the mnemonics into binary codes
"""

from typing import Optional


def dest(mnemonic: Optional[str]) -> str:
    """
    Returns the binary code of the dest mnemonic

    :param mnemonic: mnemonic to translate
    :return: str representation of 3 bits
    """

    if mnemonic is None:
        return '000'

    mnemonic = mnemonic.upper()
    binary_str: str = ''

    if 'A' not in mnemonic and 'D' not in mnemonic and 'M' not in mnemonic:
        raise CodeException('Not a C command type')

    if 'A' in mnemonic:
        binary_str += '1'
    else:
        binary_str += '0'

    if 'D' in mnemonic:
        binary_str += '1'
    else:
        binary_str += '0'

    if 'M' in mnemonic:
        binary_str += '1'
    else:
        binary_str += '0'

    return binary_str


def comp(mnemonic: str) -> str:  # pylint: disable=too-many-branches
    """
    Returns the binary code of the comp mnemonic

    :param mnemonic: mnemonic to translate
    :return: str representation of 7 bits
    """
    mnemonic_upper: str = mnemonic.upper()
    binary_str: str = ''

    if 'M' in mnemonic_upper:
        binary_str += '1'
        mnemonic_upper = mnemonic_upper.replace('M', 'A')
    else:
        binary_str += '0'

    if mnemonic_upper == '0':
        binary_str += '101010'
    elif mnemonic_upper == '1':
        binary_str += '111111'
    elif mnemonic_upper == '-1':
        binary_str += '111010'
    elif mnemonic_upper == 'D':
        binary_str += '001100'
    elif mnemonic_upper == 'A':
        binary_str += '110000'
    elif mnemonic_upper == '!D':
        binary_str += '001101'
    elif mnemonic_upper == '!A':
        binary_str += '110001'
    elif mnemonic_upper == '-D':
        binary_str += '001111'
    elif mnemonic_upper == '-A':
        binary_str += '110011'
    elif mnemonic_upper == 'D+1':
        binary_str += '011111'
    elif mnemonic_upper == 'A+1':
        binary_str += '110111'
    elif mnemonic_upper == 'D-1':
        binary_str += '001110'
    elif mnemonic_upper == 'A-1':
        binary_str += '110010'
    elif mnemonic_upper == 'D+A':
        binary_str += '000010'
    elif mnemonic_upper == 'D-A':
        binary_str += '010011'
    elif mnemonic_upper == 'A-D':
        binary_str += '000111'
    elif mnemonic_upper == 'D&A':
        binary_str += '000000'
    elif mnemonic_upper == 'D|A':
        binary_str += '010101'

    return binary_str


def jump(mnemonic: Optional[str]) -> str:
    """
    Returns the binary code of the jump mnemonic

    :param mnemonic: mnemonic to translate
    :return: str representation of 3 bits
    """
    if mnemonic is None:
        return '000'

    mnemonic = mnemonic.upper()
    binary_str = ''
    if mnemonic == 'JGT':
        binary_str = '001'
    elif mnemonic == 'JEQ':
        binary_str = '010'
    elif mnemonic == 'JGE':
        binary_str = '011'
    elif mnemonic == 'JLT':
        binary_str = '100'
    elif mnemonic == 'JNE':
        binary_str = '101'
    elif mnemonic == 'JLE':
        binary_str = '110'
    elif mnemonic == 'JMP':
        binary_str = '111'

    if len(binary_str) == 3:
        return binary_str

    raise CodeException(f'Jump command not formatted correctly: {mnemonic}')


class CodeException(Exception):
    """
    Custom exception for the code module
    """
    pass
