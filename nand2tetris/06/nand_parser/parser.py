"""
Parses an asm file and returns the correct command for each line
"""

from enum import Enum, auto
from typing import List, Optional


class CommandType(Enum):
    """
    Enum of the command type, A,C, or L
    """
    A_COMMAND = auto()
    C_COMMAND = auto()
    L_COMMAND = auto()


class Parser(object):
    """
    Object that keeps the state of the asm file and
    implements the file IO -> command conversion
    """
    def __init__(self, filename: str) -> None:
        self.filename = filename
        self.file_stream = None
        self.lines: List[str] = []
        self.current_line: str = ''
        self.line_number = -1

        with open(self.filename) as file:
            all_lines: List[str] = file.readlines()

        for line in all_lines:
            test_line: str = line.replace('\n', '').strip()

            # remove comments
            comment_index = test_line.find('//')
            if comment_index != -1:
                test_line = test_line[:comment_index].strip()
            if test_line:
                self.lines.append(test_line)

    def has_more_commands(self) -> bool:
        """
        Returns true if the file has more commands to parse, false otherwise

        :return: True if file has more commands, false otherwise
        """
        return len(self.lines) != 0

    def advance(self) -> None:
        """
        Advances the parser to the next command
        """
        if self.get_current_command_type() == CommandType.L_COMMAND:
            self.line_number -= 1
        if self.has_more_commands():
            temp_str = self.lines.pop(0)
            self.current_line = ''.join(temp_str.split())
            self.line_number += 1

    def get_current_command_type(self) -> CommandType:
        """
        Get the command type of the current command in the parser

        :return: CommandType
        """
        if '@' in self.current_line:
            return CommandType.A_COMMAND
        elif '(' in self.current_line:
            return CommandType.L_COMMAND

        return CommandType.C_COMMAND

    def get_symbol(self) -> str:
        """
        Returns the symbol or decimal Xxx of the current command @Xxx or (Xxx)

        :return: Symbol or decimal in String
        :raise Exception: if current command is not an L or A command
        """
        command_type = self.get_current_command_type()

        if command_type == CommandType.A_COMMAND:
            return self.current_line[(self.current_line.find('@')+1):]
        if command_type == CommandType.L_COMMAND:
            return self.current_line[
                (self.current_line.find('(')+1):(self.current_line.find(')'))]

        raise ParserException('get_symbol should not be called with a '
                              'C command type (or unknown)')

    def get_dest(self) -> Optional[str]:
        """
        Returns the dest mnemonic in the current C-command (8 possiblities)

        :return: Dest command, or None if none found
        :raise Exception: if current command is not a C command
        """

        if self.get_current_command_type() != CommandType.C_COMMAND:
            raise ParserException('get_dest should only be called with a '
                                  'C command type')

        if '=' in self.current_line:
            return self.current_line[:self.current_line.find('=')]

        return None

    def get_comp(self) -> str:
        """
        Returns the comp mnemonic in the current C-command (28 possibilities)

        :return: Comp command
        :raise Exception: if current command is not a C command
        """
        if self.get_current_command_type() != CommandType.C_COMMAND:
            raise ParserException('get_dest should only be called with a '
                                  'C command type')

        temp_current_line: str = self.current_line
        if '=' in temp_current_line:
            temp_current_line = \
                temp_current_line[temp_current_line.find('=')+1:]
        if ';' in temp_current_line:
            temp_current_line = temp_current_line[:temp_current_line.find(';')]

        return temp_current_line

    def get_jump(self) -> Optional[str]:
        """
        Returns the jump mnemonic in the current C-command (8 possibilities)

        :return: Jump command if exists, None otherwise
        :raise Exception: if current command is not a C command
        """
        if self.get_current_command_type() != CommandType.C_COMMAND:
            raise ParserException('get_dest should only be called with a '
                                  'C command type')
        if ';' in self.current_line:
            return self.current_line[self.current_line.find(';')+1:]

        return None


class ParserException(Exception):
    """
    Custom exception for the Parser class
    """
    pass
