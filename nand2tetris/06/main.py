"""
Takes a hack assembly file and outputs the binary format for the hack computer
"""
import os
import argparse

from nand_parser.parser import Parser, CommandType
from nand_code import code
from nand_symbol.symbol import NandSymbol


def main():
    """
    Main program loop
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('filename', type=str, help='file to create the binary file from')
    parser.add_argument('--output', type=str, default=None,
                        help='What file to write to, defaults to [filename].hack')

    args = parser.parse_args()

    if args.output is None:
        new_file_name = '{}.hack'.format(os.path.splitext(args.filename)[0])
    else:
        new_file_name = args.output

    # first run
    symbol_parser = Parser(args.filename)
    symbol_table = NandSymbol()

    while symbol_parser.has_more_commands():
        symbol_parser.advance()
        command_type = symbol_parser.get_current_command_type()
        if command_type == CommandType.L_COMMAND:
            symbol = symbol_parser.get_symbol()
            if symbol not in symbol_table.symbols:
                symbol_table.symbols[symbol] = symbol_parser.line_number

    file_parser = Parser(args.filename)
    current_memory = 16
    with open(new_file_name, 'w') as file:
        while file_parser.has_more_commands():
            file_parser.advance()
            command_type = file_parser.get_current_command_type()
            binary_command: str
            if command_type == CommandType.C_COMMAND:
                dest = file_parser.get_dest()
                comp = file_parser.get_comp()
                jump = file_parser.get_jump()
                binary_command = f'111{code.comp(comp)}{code.dest(dest)}{code.jump(jump)}'
            elif command_type == CommandType.A_COMMAND:
                symbol = file_parser.get_symbol()
                int_symbol = symbol_table.symbols.get(symbol)
                if int_symbol is None:
                    int_symbol = symbol
                    try:
                        int(int_symbol)
                    except ValueError:
                        int_symbol = current_memory
                        current_memory += 1
                        symbol_table.symbols[symbol] = int_symbol
                binary_command = '0{:015b}'.format(int(int_symbol))
            else:
                continue
            file.write(f'{binary_command}\n')


if __name__ == '__main__':
    main()
