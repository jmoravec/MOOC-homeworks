"""
Test module for the Parser class
"""
import os
from typing import Optional
import pytest

from nand_parser.parser import Parser, CommandType, ParserException

FILE_PATH = os.path.dirname(os.path.realpath(__file__))


@pytest.mark.parametrize("filename,actual_lines",
                         [('test_files/remove_comment_whitespace.asm', 2)])
def test_remove_comments_from_file(filename: str, actual_lines: int):
    """
    Tests that comments are removed from a file
    """
    test_parser = Parser(f'{FILE_PATH}/{filename}')
    line: str
    for line in test_parser.lines:
        assert '#' not in line
        assert ' ' not in line
    assert len(test_parser.lines) == actual_lines


@pytest.mark.parametrize("filename,expected",
                         [('test_files/remove_comment_whitespace.asm', True),
                          ('test_files/blank_file.asm', False)])
def test_has_more_commands(filename: str, expected: bool):
    """
    Tests that the has more commands function returns the expected behavior
    """
    test_parser = Parser(f'{FILE_PATH}/{filename}')
    assert test_parser.has_more_commands() == expected


@pytest.mark.parametrize("filename,expected_commands",
                         [('test_files/remove_comment_whitespace.asm', 2)])
def test_advance_polls_the_file(filename: str, expected_commands: int):
    """
    Tests that the advance command pools the entire file
    """
    test_parser = Parser(f'{FILE_PATH}/{filename}')
    i: int = 0
    while test_parser.has_more_commands():
        test_parser.advance()
        i += 1
    assert i == expected_commands


@pytest.mark.parametrize("filename,expected_command_type",
                         [('test_files/only_a_commands.asm',
                           CommandType.A_COMMAND),
                          ('test_files/only_l_commands.asm',
                           CommandType.L_COMMAND),
                          ('test_files/only_c_commands.asm',
                           CommandType.C_COMMAND)])
def test_get_current_command_type(filename: str,
                                  expected_command_type: CommandType):
    """
    Tests that the get_current_command_type call
    correctly returns the correct type
    """
    test_parser = Parser(f'{FILE_PATH}/{filename}')
    while test_parser.has_more_commands():
        test_parser.advance()
        assert test_parser.get_current_command_type() == expected_command_type


@pytest.mark.parametrize('command,expected_symbol',
                         [('@1', '1'),
                          ('@R1', 'R1'),
                          ('@TEST', 'TEST'),
                          ('(END)', 'END'),
                          ('(LOOP)', 'LOOP')])
def test_get_symbol(command: str, expected_symbol: str):
    """
    Tests that the get_symbol command correctly returns
    the expected symbol
    """
    test_parser = Parser(f'{FILE_PATH}/test_files/blank_file.asm')
    test_parser.current_line = command
    assert test_parser.get_symbol() == expected_symbol


@pytest.mark.parametrize('command',
                         ['A=D;JMP', 'A=1', 'D+1'])
def test_get_symbol_error(command: str):
    """
    Tests that the get_symbol command throws an exception when
    trying to call on a c command
    """
    test_parser = Parser(f'{FILE_PATH}/test_files/blank_file.asm')
    test_parser.current_line = command
    try:
        test_parser.get_symbol()
        assert False, f'C command did not throw an exception: {command}'
    except ParserException:
        pass


@pytest.mark.parametrize('command,expected',
                         [('D+1', None),
                          ('M=A-1', 'M'),
                          ('D=D&A', 'D'),
                          ('MD=-A', 'MD'),
                          ('A=D|M', 'A'),
                          ('AM=0', 'AM'),
                          ('AD=!M', 'AD'),
                          ('AMD=D+1', 'AMD'),
                          ('D=D|M;JMP', 'D')])
def test_get_dest(command: str, expected: Optional[str]):
    """
    Tests that the get_dest command correctly returns the expected symbol
    """
    test_parser = Parser(f'{FILE_PATH}/test_files/blank_file.asm')
    test_parser.current_line = command
    assert test_parser.get_dest() == expected


@pytest.mark.parametrize('command', ['@R1', '@5', '@LOOP', '(LOOP)'])
def test_get_dest_error(command: str):
    """
    Tests that the get_dest command throws an exception when trying to
    call on an A or L command
    """
    test_parser = Parser(f'{FILE_PATH}/test_files/blank_file.asm')
    test_parser.current_line = command
    try:
        test_parser.get_dest()
        assert False, f'A or L command did not throw an exception: {command}'
    except ParserException:
        pass


@pytest.mark.parametrize('command,expected',
                         [('D+1', 'D+1'),
                          ('M=A-1', 'A-1'),
                          ('D=D&A', 'D&A'),
                          ('MD=-A', '-A'),
                          ('A=D|M', 'D|M'),
                          ('AM=0', '0'),
                          ('AD=!M', '!M'),
                          ('AMD=D+1', 'D+1'),
                          ('D=D|M;JMP', 'D|M')])
def test_get_comp(command: str, expected: str):
    """
    Tests that the get_comp command correctly returns the expected symbol
    """
    test_parser = Parser(f'{FILE_PATH}/test_files/blank_file.asm')
    test_parser.current_line = command
    assert test_parser.get_comp() == expected


@pytest.mark.parametrize('command', ['@R1', '@5', '@LOOP', '(LOOP)'])
def test_get_comp_error(command: str):
    """
    Tests that the get_dest command throws an exception when trying to
    call on an A or L command
    """
    test_parser = Parser(f'{FILE_PATH}/test_files/blank_file.asm')
    test_parser.current_line = command
    try:
        test_parser.get_comp()
        assert False, f'A or L command did not throw an exception: {command}'
    except ParserException:
        pass


@pytest.mark.parametrize('command,expected',
                         [('D+1', None),
                          ('M=A-1', None),
                          ('D=D&A;JGT', 'JGT'),
                          ('MD=-A;JEQ', 'JEQ'),
                          ('A=D|M;JGE', 'JGE'),
                          ('AM=0;JLT', 'JLT'),
                          ('AD=!M;JNE', 'JNE'),
                          ('AMD=D+1;JLE', 'JLE'),
                          ('D=D|M;JMP', 'JMP')])
def test_get_jump(command: str, expected: str):
    """
    Tests that the get_comp command correctly returns the expected symbol
    """
    test_parser = Parser(f'{FILE_PATH}/test_files/blank_file.asm')
    test_parser.current_line = command
    assert test_parser.get_jump() == expected


@pytest.mark.parametrize('command', ['@R1', '@5', '@LOOP', '(LOOP)'])
def test_get_jump_error(command: str):
    """
    Tests that the get_dest command throws an exception when trying to
    call on an A or L command
    """
    test_parser = Parser(f'{FILE_PATH}/test_files/blank_file.asm')
    test_parser.current_line = command
    try:
        test_parser.get_jump()
        assert False, f'A or L command did not throw an exception: {command}'
    except ParserException:
        pass
