"""
Takes a Jack File/directory and outputs the file into the Hack VM language
"""
import os
import argparse
import ntpath

from typing import List, Tuple
from tokenizer.tokenizer import Tokenizer, TokenType
from compilation_engine.compilation_engine import CompilationEngine


def main():
    """
    Main program loop
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('filename', type=str, help='Jack input file or directory')
    parser.add_argument('--output', type=str, default=None,
                        help='File to write output too, defaults to [filename].asm')
    args = parser.parse_args()

    is_dir = os.path.isdir(args.filename)
    # file_without_path = ntpath.basename(args.filename)
    tokens: List[Tuple[TokenType, str]] = []

    files_to_parse: List[str] = []
    if is_dir:
        for file in os.listdir(args.filename):
            if file.endswith('.jack'):
                files_to_parse.append(f'{args.filename}/{file}')
    else:
        files_to_parse.append(args.filename)

    # token_element = ET.Element('tokens')
    for file_name in files_to_parse:
        file_without_path = ntpath.basename(file_name)
        new_file_name = '{}.xml'.format(os.path.splitext(file_without_path)[0])

        token_parser = Tokenizer(file_name)
        while token_parser.has_more_tokens():
            token_parser.advance()
            token_type: TokenType = token_parser.get_token_type()
            # sub_element = ET.SubElement(token_element, token_type.value)
            # sub_element.text = f' {value} '
            token_value = token_parser.get_value()
            tokens.append((token_type, token_value))

        # with open(new_file_name, 'w') as file:
            # file.write(ET.tostring(token_element).decode('utf8'))
            #
        compiler = CompilationEngine(new_file_name, tokens)
        while tokens:
            compiler.compile_class()
            compiler.element_tree.write(new_file_name)


if __name__ == '__main__':
    main()
