"""
Removes all comments and white space from the input
stream and breaks it into Jack-language tokens, as specified by the Jack grammar.
"""

from typing import List
from enum import Enum
import re

JACK_SYMBOLS = ['{', '}', '(', ')', '[', ']', '.', ',', ';', '+', '-', '*',
                '/', '&', '|', '<', '>', '=', '~']


class TokenType(Enum):
    """
    The various token types
    """
    KEYWORD = 'keyword'
    SYMBOLS = 'symbol'
    IDENTIFIER = 'identifier'
    INT_CONST = 'integerConstant'
    STRING_CONST = 'stringConstant'


class KeywordType(Enum):
    """
    Various Keywords in Jack
    """
    CLASS = 'class'
    METHOD = 'method'
    FUNCTION = 'function'
    CONSTRUCTOR = 'constructor'
    INT = 'int'
    BOOLEAN = 'boolean'
    CHAR = 'char'
    VOID = 'void'
    VAR = 'var'
    STATIC = 'static'
    FIELD = 'field'
    LET = 'let'
    DO = 'do'
    IF = 'if'
    ELSE = 'else'
    WHILE = 'while'
    RETURN = 'return'
    TRUE = 'true'
    FALSE = 'false'
    NULL = 'null'
    THIS = 'this'

    @classmethod
    def has_value(cls, value: str) -> bool:
        """
        Returns true if value is in the enum list, false otherwise
        """
        return any(value == item.value for item in cls)


class Tokenizer():
    """
    Removes all comments and white space from the input
    stream and breaks it into Jack-language tokens, as specified by the Jack grammar.
    """

    @staticmethod
    def _remove_comments(string: str):
        pattern = r"(\".*?\"|\'.*?\')|(/\*.*?\*/|//[^\r\n]*$)"
        regex = re.compile(pattern, re.MULTILINE | re.DOTALL)

        def _replacer(match):
            if match.group(2) is not None:
                return ""
            return match.group(1)
        return regex.sub(_replacer, string)

    def __init__(self, filename: str) -> None:
        self.filename = filename
        self.file_stream = None
        self.lines: List[str] = []
        self.tokens: List[str] = []
        self.current_token: str = ''

        with open(self.filename) as file:
            content: str = file.read()

        # remove multi line comments
        no_comments = self._remove_comments(content)
        all_lines: List[str] = no_comments.split('\n')

        for line in all_lines:
            test_line: str = line.replace('\n', '').strip()

            # remove inline comments
            comment_index = test_line.find('//')
            if comment_index != -1:
                test_line = test_line[:comment_index].strip()

            if test_line:
                self.lines.append(test_line)

    def has_more_tokens(self) -> bool:
        """
        Are there more tokens to parse

        :return: True if more tokens, False otherwise
        """
        if self.lines:
            return True

        if self.tokens:
            return True

        return False

    def advance(self) -> None:
        """
        Gets the next token from the input and makes it the current token.
        This method should only be called if has_more_tokens() is True.
        Initially there is no current token.
        """
        if not self.tokens:
            if self.has_more_tokens():
                self.tokens = self._parse(self.lines.pop(0))
            else:
                raise NoMoreTokensException()
        self.current_token = self.tokens.pop(0)

    def get_token_type(self) -> TokenType:
        """
        Returns the type of the current token
        """
        if KeywordType.has_value(self.current_token):
            return TokenType.KEYWORD
        if self.current_token in JACK_SYMBOLS:
            return TokenType.SYMBOLS
        try:
            int(self.current_token)
            return TokenType.INT_CONST
        except ValueError:
            pass
        if self.current_token[0] == '"':
            return TokenType.STRING_CONST

        return TokenType.IDENTIFIER

    def get_keyword(self) -> KeywordType:
        """
        Returns the keyword which is the current token. Should be called only
        when tokenType() is KEYWORD
        """
        if self.get_token_type() != TokenType.KEYWORD:
            raise TokenMismatchException()
        return KeywordType(self.current_token)

    def get_symbol(self) -> str:
        """
        Returns the character which is the current token. Should be called
        only when tokenType() is SYMBOL
        """
        if self.get_token_type() != TokenType.SYMBOLS:
            raise TokenMismatchException()
        return self.current_token

    def get_identifier(self) -> str:
        """
        Returns the identifier which is the current token. Should be called only
        when token_type is IDENTIFIER
        """
        if self.get_token_type() != TokenType.IDENTIFIER:
            raise TokenMismatchException()
        return str(self.current_token)

    def get_int_val(self) -> int:
        """
        Returns the integer value of the current token. Should be called only
        when token_type is INT_CONST
        """
        if self.get_token_type() != TokenType.INT_CONST:
            raise TokenMismatchException()
        return int(self.current_token)

    def get_string_val(self) -> str:
        """
        Returns the string value of the current token, without the double quotes.
        Should be called only when token_type is STRING_CONST
        """
        if self.get_token_type() != TokenType.STRING_CONST:
            raise TokenMismatchException()
        return str(self.current_token[1:-1])

    def get_value(self) -> str:
        """
        Get the value of the current token, no matter what the token type is
        """
        token_type = self.get_token_type()
        if token_type == TokenType.KEYWORD:
            return self.get_keyword().value
        if token_type == TokenType.SYMBOLS:
            return self.get_symbol()
        if token_type == TokenType.IDENTIFIER:
            return self.get_identifier()
        if token_type == TokenType.INT_CONST:
            return str(self.get_int_val())

        return self.get_string_val()

    @classmethod
    def _parse(cls, line: str) -> List[str]:
        tokens: List[str] = []
        current_token = ''
        is_string = False
        for char in line:
            if char == '"':
                if is_string:
                    current_token += char
                    is_string = False
                    tokens.append(current_token)
                    current_token = ''
                    continue
                else:
                    is_string = True
                    current_token += char
            elif char == ' ':
                if current_token and not is_string:
                    tokens.append(current_token)
                    current_token = ''
                    continue
                elif is_string:
                    current_token += char
            elif char in JACK_SYMBOLS and not is_string:
                if current_token != '':
                    tokens.append(current_token)
                    current_token = ''
                tokens.append(char)
                continue
            else:
                current_token += char
        if current_token != '':
            tokens.append(current_token)
        return tokens


class NoMoreTokensException(Exception):
    """
    Exception when no more tokens are left to be processed but code tries to get next one
    """
    pass


class TokenMismatchException(Exception):
    """
    Exception when attempting to get a token value by the wrong type
    """
    pass
