// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Mult.asm

// Multiplies R0 and R1 and stores the result in R2.
// (R0, R1, R2 refer to RAM[0], RAM[1], and RAM[2], respectively.)

// Put your code here.

	//i = 1
	@i
	M=0

	//sum = 0
	@sum
	M=0

	//if R0 == 0, end
	@R0
	D=M
	@FINISH
	D;JEQ

	//if R1 == 0, end
	@R1
	D=M
	@FINISH
	D;JEQ

(LOOP)
	// sum += R0
	@R0
	D=M
	@sum
	M=D+M

	@i
	MD=M+1

	@R1
	D=D-M
	@LOOP
	D;JLT

	@sum
	D=M

(FINISH)
	@R2
	M=D

(END)
	@END
	0;JMP