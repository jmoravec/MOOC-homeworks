// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Fill.asm

// Runs an infinite loop that listens to the keyboard input.
// When a key is pressed (any key), the program blackens the screen,
// i.e. writes "black" in every pixel;
// the screen should remain fully black as long as the key is pressed.
// When no key is pressed, the program clears the screen, i.e. writes
// "white" in every pixel;
// the screen should remain fully clear as long as no key is pressed.

// Put your code here.
//setup const
	@8192
	D=A
	@end
	M=D
	//@256
	//D=A
	//@row_end
	//M=D

(LOOP)
	// get keyboard key
	@KBD
	D=M

	// if KBD != 0, go to black section
	@BLACK
	D;JNE

	// otherwise go to white
	@WHITE
	0;JMP

(BLACK)
	@color
	M=-1

	@FILL
	0;JMP

(WHITE)
	@color
	M=0

	@FILL
	0;JMP

(FILL)
	@i
	M=0
	//@j
	//M=0
	@SCREEN
	D=A
	//@inner_screen
	//M=D

//(ROW_FILL)
	//@j




(FILL_LOOP)
	@SCREEN
	D=A

	@i
	A=M+D
	D=A

	@pixel
	M=D

	@color
	D=M

	@pixel
	A=M
	M=D

	@i
	MD=M+1

	@end
	D=M-D

	@FILL_LOOP
	D;JGT

	@LOOP
	0;JMP