"""
Takes a Hack VM file and outputs the Hack Assembly file
"""
import os
import argparse
import ntpath
from typing import List

from vm_parser.parser import Parser, CommandType
from code_writer.code_writer import CodeWriter


def main():
    """
    Main program loop
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('filename', type=str, help='VM input file or directory')
    parser.add_argument('--output', type=str, default=None,
                        help='File to write output too, defaults to [filename].asm')
    args = parser.parse_args()

    is_dir = os.path.isdir(args.filename)
    file_without_path = ntpath.basename(args.filename)

    if args.output is None:
        new_file_name = '{}.asm'.format(os.path.splitext(file_without_path)[0])
    else:
        new_file_name = args.output

    files_to_parse: List[str] = []
    if is_dir:
        for file in os.listdir(args.filename):
            if file.endswith('.vm'):
                files_to_parse.append(f'{args.filename}{file}')
    else:
        files_to_parse.append(args.filename)

    commands_to_write = _get_commands(files_to_parse)

    with open(new_file_name, 'w') as file:
        file.write(commands_to_write)


def _get_commands(files_to_parse: List[str]) -> str:
    all_commands: str = CodeWriter('bootstrap').get_init(0)
    function_call_num = 1
    for file_name in files_to_parse:
        base_file = os.path.splitext(ntpath.basename(file_name))[0]
        function_name = f'{base_file}'
        translator = CodeWriter(file_name)
        main_parser = Parser(file_name)

        while main_parser.has_more_commands():
            main_parser.advance()
            command_type = main_parser.get_command_type()
            all_commands += f'//{main_parser.current_line}\n'
            print(main_parser.current_line)
            if command_type == CommandType.C_ARITHMETIC:
                all_commands += translator.get_arithmetic(main_parser.get_arg1())
            elif command_type in [CommandType.C_POP, CommandType.C_PUSH]:
                all_commands += translator.get_push_pop(command_type,
                                                        main_parser.get_arg1(),
                                                        main_parser.get_arg2())
            elif command_type == CommandType.C_LABEL:
                all_commands += translator.get_label(function_name, main_parser.get_arg1())
            elif command_type == CommandType.C_GOTO:
                all_commands += translator.get_goto(function_name, main_parser.get_arg1())
            elif command_type == CommandType.C_IF:
                all_commands += translator.get_if(function_name, main_parser.get_arg1())
            elif command_type == CommandType.C_CALL:
                all_commands += translator.get_call(main_parser.get_arg1(),
                                                    main_parser.get_arg2(),
                                                    function_call_num)
                function_call_num += 1
            elif command_type == CommandType.C_FUNCTION:
                function_name = main_parser.get_arg1()
                all_commands += translator.get_function(function_name, main_parser.get_arg2())
            elif command_type == CommandType.C_RETURN:
                all_commands += translator.get_return()

    return all_commands


if __name__ == '__main__':
    main()
