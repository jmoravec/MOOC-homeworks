"""
Module for methods that convert the VM commands to Hack assembly language
"""
import os
import ntpath
from vm_parser.parser import CommandType


class CodeWriter():
    """
    Class that has methods to convert MV commands to Hack assembly
    """
    segment_translation = {
        'argument': 'ARG',
        'local': 'LCL',
        'this': 'THIS',
        'that': 'THAT',
        'pointer': 'R3',
        'temp': 'R5'
    }

    def __init__(self, filename: str) -> None:
        self.arith_jump: int = 0
        self.base_file = os.path.splitext(ntpath.basename(filename))[0]

    @classmethod
    def _pop(cls) -> str:
        """
        Helper method that pops the stack in assembly
        """
        return '@SP\nAM=M-1\nD=M\n'

    @classmethod
    def _push_what(cls, what: str) -> str:
        return f'@SP\nA=M\nM={what}\n@SP\nM=M+1\n'

    @classmethod
    def _push_d(cls) -> str:
        """
        Helper method that pushs D to the stack
        """
        return cls._push_what('D')

    def get_arithmetic(self, command: str) -> str:
        """
        Converts add, sub, neg, eq, gt, lt, and, or, not to assembly
        """
        vm_command = ''
        if command in ['add', 'sub', 'and', 'or']:
            if 'add' in command:
                opp = '+'
            elif 'sub' in command:
                opp = '-'
            elif 'and' in command:
                opp = '&'
            elif 'or' in command:
                opp = '|'

            vm_command = self._pop()
            vm_command += f'A=A-1\nD=M{opp}D\n@SP\nM=M-1\n'
            vm_command += self._push_d()
        elif command in ['eq', 'gt', 'lt']:
            jump_location = f'ARITH_JUMP.{self.arith_jump}'
            self.arith_jump += 1
            jump_command: str
            if 'eq' in command:
                jump_command = 'JEQ'
            elif 'gt' in command:
                jump_command = 'JGT'
            else:
                jump_command = 'JLT'

            vm_command = self._pop()
            vm_command += 'A=A-1\nD=M-D\n@SP\nM=M-1\n'
            vm_command += f'@{jump_location}\nD;{jump_command}\n'
            # false statement
            vm_command += self._push_what('0')
            vm_command += f'@{jump_location}.END\n0;JMP\n'
            # true statement
            vm_command += f'({jump_location})\n' + self._push_what('-1')
            vm_command += f'({jump_location}.END)\n'
        elif command in ['neg', 'not']:
            if 'neg' in command:
                opp = '-'
            else:
                opp = '!'
            vm_command = f'@SP\nA=M-1\nM={opp}M\n'

        return vm_command

    def _get_segment_value(self, segment: str, index: int) -> str:
        segment_name = self.segment_translation[segment]
        if segment in ['pointer', 'temp']:
            location = 'A'
        else:
            location = 'M'

        return f'@{index}\nD=A\n@{segment_name}\nA={location}+D\nD=M\n'

    def _get_segment_index(self, segment: str, index: int) -> str:
        if segment in ['temp', 'pointer']:
            location = 'A'
        else:
            location = 'M'
        segment_name = self.segment_translation[segment]
        return f'@{index}\nD=A\n@{segment_name}\nD={location}+D\n'

    def get_push_pop(self, command_type: CommandType, segment: str, index: int) -> str:
        """
        Gets the assembly code that is the translation of push or pop
        """
        vm_command: str = ''
        if command_type == CommandType.C_PUSH:
            if segment in self.segment_translation:
                vm_command += self._get_segment_value(segment, index)
            elif 'constant' in segment:
                vm_command += f'@{index}\nD=A\n'
            else:  # static
                vm_command += f'@{self.base_file}.static.{index}\nD=M\n'

            vm_command += self._push_d()
        else:
            if segment in self.segment_translation:
                vm_command += self._get_segment_index(segment, index)
                vm_command += '@R13\nM=D\n'
                vm_command += self._pop()
                vm_command += '@R13\nA=M\nM=D\n'
            else:  # static
                vm_command += self._pop()
                vm_command += f'@{self.base_file}.static.{index}\nM=D\n'

        return vm_command

    @classmethod
    def get_label(cls, function_name: str, label: str) -> str:
        """
        Gets the assembly code that is the translation of the label command
        """
        return f'({function_name}${label})\n'

    @classmethod
    def get_goto(cls, function_name: str, label: str) -> str:
        """
        Gets the assembly code that is the translation of the goto command
        """
        return f'@{function_name}${label}\n0;JMP\n'

    def get_if(self, function_name: str, label: str) -> str:
        """
        Gets the assembly code that is the translation of the if-goto command
        """
        return self._pop() + f'@{function_name}${label}\nD;JNE\n'

    def get_call(self, function_name: str, num_args: int, unique_num: int) -> str:
        """
        Gets the assembly code that is the translation of the call command
        """
        commands = ''
        return_label = f'{function_name}.return_call.{unique_num}'

        commands += f'//push return-addres\n@{return_label}\nD=A\n' + self._push_d()
        commands += '//push LCL\n@LCL\nD=M\n' + self._push_d()
        commands += '//push ARG\n@ARG\nD=M\n' + self._push_d()
        commands += '//push THIS\n@THIS\nD=M\n' + self._push_d()
        commands += '//push THAT\n@THAT\nD=M\n' + self._push_d()
        commands += f'//ARG=SP-n-5\n@SP\nD=M\n@{num_args}\nD=D-A\n@5\nD=D-A\n@ARG\nM=D\n'
        commands += '//LCL=SP\n@SP\nD=M\n@LCL\nM=D\n'
        commands += f'//goto {function_name}\n@{function_name}\n0;JMP\n'
        commands += f'({return_label})\n'

        return commands

    def get_function(self, function_name: str, local_vars: int) -> str:
        """
        Gets the assembly code that is the translation of the function command
        """
        commands = f'({function_name})\n'
        for _ in range(local_vars):
            commands += self._push_what('0')
        return commands

    def get_return(self) -> str:
        """
        Gets the assembly code that is the translation of the return command
        """
        commands = '//FRAME(R13)=LCL\n@LCL\nD=M\n@R13\nM=D\n'
        commands += '//RET(R14)=*(FRAME-5)\n@5\nA=D-A\nD=M\n@R14\nM=D\n'
        commands += '//*ARG=pop()\n' + self._pop() + '@ARG\nA=M\nM=D\n'
        commands += '//SP=ARG+1\n@ARG\nD=M+1\n@SP\nM=D\n'
        commands += '//THAT=*(FRAME-1)\n@R13\nA=M-1\nD=M\n@THAT\nM=D\n'
        commands += '//THIS=*(FRAME-2)\n@R13\nA=M-1\nA=A-1\nD=M\n@THIS\nM=D\n'
        commands += '//ARG=*(FRAME-3)\n@R13\nD=M\n@3\nA=D-A\nD=M\n@ARG\nM=D\n'
        commands += '//LCL=*(FRAME-4)\n@R13\nD=M\n@4\nA=D-A\nD=M\n@LCL\nM=D\n'
        commands += '//goto RET\n@R14\nA=M\n0;JMP\n'

        return commands

    def get_init(self, unique_num: int) -> str:
        """
        Gets the assembly code for the bootstrap/init code
        """
        commands = '@256\nD=A\n@SP\nM=D\n'
        commands += '@LCL\nM=-A\n'
        commands += '@ARG\nM=-A\n'
        commands += '@THIS\nM=-A\n'
        commands += '@THAT\nM=-A\n'

        commands += self.get_call('sys.init', 0, unique_num)
        return commands
